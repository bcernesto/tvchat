#import <UIKit/UIKit.h>
#import "ChatData.h"

@interface ChatRoom_VC : UIViewController< UIImagePickerControllerDelegate, UINavigationControllerDelegate> {
UIImagePickerController *img_Picker;
}
// Vista principal
@property (weak, nonatomic) IBOutlet UIView *view_Home;

// Cabecera
@property (weak, nonatomic) IBOutlet UIView *view_Header;
@property (weak, nonatomic) IBOutlet UIButton *cmd_Back;
- (IBAction)cmd_Back_clic:(UIButton*)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Title;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Subtitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgv_Icon;

// Contenido
@property (weak, nonatomic) IBOutlet UIView *view_Container;
@property (weak, nonatomic) IBOutlet UITableView *tbl_Content;
/* IBOutlets no utilizados
@property (weak, nonatomic) IBOutlet UITableViewCell *tbc_ChatMsgIn;
@property (weak, nonatomic) IBOutlet UITableViewCell *tbc_ChatMsgOut;
@property (weak, nonatomic) IBOutlet UITableViewCell *tbc_ChatImgIn;
@property (weak, nonatomic) IBOutlet UITableViewCell *tbc_ChatImgOut;
 */

// Toolbar
@property (weak, nonatomic) IBOutlet UIView *view_ToolBar;
@property (weak, nonatomic) IBOutlet UIButton *cmd_Add;
- (IBAction)cmd_Add_clic:(UIButton*)sender;
@property (weak, nonatomic) IBOutlet UITextField *txt_Message;
@property (weak, nonatomic) IBOutlet UIButton *cmd_Send;
- (IBAction)cmd_Send_clic:(UIButton*)sender;

// Otras propiedades
@property (nonatomic, strong)  NSMutableArray *chatArray;
@property (nonatomic, strong) ChatData *chatData;
@property (nonatomic, strong) NSTimer *autoReplyMessage;
@end
