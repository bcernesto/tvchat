#import "ChatStartArray.h"
#import "ChatData.h"

@implementation ChatStartArray

-(id) initChatArray{
ChatData *chat1 = [[ChatData alloc]init];
chat1.m_iVersion=1;
chat1.m_iID=0;
chat1.m_bIsMine=YES;
chat1.m_eChatDataType=ChatData_Message;
chat1.m_sMessage=@"Hola";
chat1.m_Date=[self nsDateFromString:@"26/01/2017"];
chat1.m_Image=nil;

ChatData *chat2 = [[ChatData alloc]init];
chat2.m_iVersion=1;
chat2.m_iID=1;
chat2.m_bIsMine=NO;
chat1.m_eChatDataType=ChatData_Message;
chat2.m_sMessage=@"Hola, qué tal?";
chat2.m_Date=[self nsDateFromString:@"26/01/2017"];
chat2.m_Image=nil;

ChatData *chat3 = [[ChatData alloc]init];
chat3.m_iVersion=1;
chat3.m_iID=2;
chat3.m_bIsMine=YES;
chat3.m_eChatDataType=ChatData_Message;
chat3.m_sMessage=@"Pásame las fotos";
chat3.m_Date=[self nsDateFromString:@"26/01/2017"];
chat3.m_Image=nil;

ChatData *chat4 = [[ChatData alloc]init];
chat4.m_iVersion=1;
chat4.m_iID=3;
chat4.m_bIsMine=NO;
chat4.m_eChatDataType=ChatData_Image;
chat4.m_sMessage=@"La primera";
chat4.m_Date=[self nsDateFromString:@"27/01/2017"];
chat4.m_Image=[UIImage imageNamed:@"image1.jpg"];

ChatData *chat5 = [[ChatData alloc]init];
chat5.m_iVersion=1;
chat5.m_iID=4;
chat5.m_bIsMine=NO;
chat5.m_eChatDataType=ChatData_Image;
chat5.m_sMessage=@"Otra";
chat5.m_Date=[self nsDateFromString:@"27/01/2017"];
chat5.m_Image=[UIImage imageNamed:@"image2.jpg"];

ChatData *chat6 = [[ChatData alloc]init];
chat6.m_iVersion=1;
chat6.m_iID=5;
chat6.m_bIsMine=NO;
chat6.m_eChatDataType=ChatData_Image;
chat6.m_sMessage=@"Otra más";
chat6.m_Date=[self nsDateFromString:@"27/01/2017"];
chat6.m_Image=[UIImage imageNamed:@"image3.jpg"];

ChatData *chat7 = [[ChatData alloc]init];
chat7.m_iVersion=1;
chat7.m_iID=6;
chat7.m_bIsMine=NO;
chat7.m_eChatDataType=ChatData_Image;
chat7.m_sMessage=@"La última";
chat7.m_Date=[self nsDateFromString:@"27/01/2017"];
chat7.m_Image=[UIImage imageNamed:@"image4.jpg"];

ChatData *chat8 = [[ChatData alloc]init];
chat8.m_iVersion=1;
chat8.m_iID=7;
chat8.m_bIsMine=NO;
chat8.m_eChatDataType=ChatData_Message;
chat8.m_sMessage=@"Pásame las otras";
chat8.m_Date=[self nsDateFromString:@"27/01/2017"];
chat8.m_Image=nil;

NSMutableArray *chatArray = [NSMutableArray arrayWithObjects:chat1, chat2, chat3, chat4, chat5, chat6, chat7, chat8, nil];
return chatArray;
}

-(NSDate*) nsDateFromString:(NSString*)dateSTR{
NSDateFormatter *spain = [[NSDateFormatter alloc] init];
spain.timeStyle = NSDateFormatterNoStyle;
spain.dateFormat = @"dd/MM/yyyy";
NSDate *date = [spain dateFromString:dateSTR];
return date;
}

@end
