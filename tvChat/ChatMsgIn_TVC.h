#import <UIKit/UIKit.h>

@interface ChatMsgIn_TVC : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img_GloboA;
@property (weak, nonatomic) IBOutlet UIImageView *img_GloboB;
@property (weak, nonatomic) IBOutlet UIImageView *img_GloboC;
@property (weak, nonatomic) IBOutlet UIImageView *img_GloboD;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Message;
@end
