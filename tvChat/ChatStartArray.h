#import <Foundation/Foundation.h>

@interface ChatStartArray : NSObject
-(id) initChatArray;
-(NSDate*) nsDateFromString:(NSString*)dateSTR;
@end
