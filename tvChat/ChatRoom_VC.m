#import "ChatRoom_VC.h"
#import "ChatStartArray.h"
#import "ChatMsgIn_TVC.h"
#import "ChatMsgOut_TVC.h"
#import "ChatImgIn_TVC.h"
#import "ChatImgOut_TVC.h"

@interface ChatRoom_VC (){
float anchoVentana;
float altoVentana;
}@end

@implementation ChatRoom_VC

- (void)viewDidLoad {
    [super viewDidLoad];
	// [self setViewsSize];
	[self setViewAtributes];
	_chatArray=[[ChatStartArray alloc]initChatArray];
}

- (void) viewDidAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
	[super viewDidAppear:animated];
	}

- (void)viewDidDisappear:(BOOL)animated{
        [[NSNotificationCenter defaultCenter] removeObserver:self  name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self  name:UIKeyboardDidShowNotification object:nil];
    [super viewDidDisappear:animated];
}

-(void) setViewsSize{
anchoVentana=_view_Home.bounds.size.width;
    altoVentana=_view_Home.bounds.size.height;

// Header
_view_Header.frame = CGRectMake(0, 0, anchoVentana, altoVentana*0.14);
float anchoHeader=_view_Header.bounds.size.width;
    float altoHeader=_view_Header.bounds.size.height;
_cmd_Back.frame = CGRectMake(0, 0, anchoHeader*0.2, altoHeader);
_lbl_Title.frame = CGRectMake(anchoHeader*0.2, 0, anchoHeader*0.6, altoHeader/2);
_lbl_Subtitle.frame = CGRectMake(anchoHeader*0.2, altoHeader/2, anchoHeader*0.6, altoHeader/2);
_imgv_Icon.frame = CGRectMake(anchoHeader*0.8, 0, anchoHeader*0.2, altoHeader);

// Contenido
_view_Container.frame = CGRectMake(0, altoVentana*0.14, anchoVentana, altoVentana*0.72);
_tbl_Content.frame = CGRectMake(0, 0, _view_Container.bounds.size.width, _view_Container.bounds.size.height);

// ToolBar
_view_ToolBar.frame = CGRectMake(0, altoVentana*0.86, anchoVentana, altoVentana*0.14);
float anchoToolBar=_view_ToolBar.bounds.size.width;
    float altoToolBar=_view_ToolBar.bounds.size.height;
_cmd_Add.frame = CGRectMake(0, altoToolBar*0.1, altoToolBar*0.8, altoToolBar*0.8);
_txt_Message.frame = CGRectMake(anchoToolBar*0.25, altoToolBar*0.1, anchoToolBar*0.5, altoToolBar*0.8);
_cmd_Send.frame = CGRectMake(anchoToolBar*0.75, altoToolBar*0.1, anchoToolBar*0.25, altoToolBar*0.8);
}

-(void) setViewAtributes{
[_imgv_Icon setIsAccessibilityElement:TRUE];
  [_imgv_Icon setAccessibilityLabel:@"Icono tvChat"];
  [_cmd_Back setAccessibilityLabel:@"Volver"];
  [_cmd_Add setAccessibilityLabel:@"Adjuntar"];
    _tbl_Content.delegate=self;
    _tbl_Content.dataSource=self;
}

- (IBAction)cmd_Back_clic:(UIButton*)sender{
[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)cmd_Add_clic:(UIButton*)sender{
UIAlertController* atachImage = [UIAlertController alertControllerWithTitle:@"Seleccionar imagen desde"
                               message:@"Desde donde quieres obtener la imagen?"
                               preferredStyle:UIAlertControllerStyleActionSheet];
							   UIAlertAction* camera = [UIAlertAction
                     actionWithTitle:@"Camara"
                     style:UIAlertActionStyleDefault
                     handler:^(UIAlertAction * action)                     {
					 [atachImage dismissViewControllerAnimated:YES completion:nil];
                         [self showCamera];
                     }];
[atachImage addAction:camera];
UIAlertAction* library = [UIAlertAction
                     actionWithTitle:@"Biblioteca"
                     style:UIAlertActionStyleDefault
                     handler:^(UIAlertAction * action)                     {
					 [atachImage dismissViewControllerAnimated:YES completion:nil];
                         [self showLibrary];
                     }];
[atachImage addAction:library];
UIAlertAction* cancel = [UIAlertAction
                     actionWithTitle:@"Cancelar"
                     style:UIAlertActionStyleDefault
                     handler:^(UIAlertAction * action)                     {
					 [atachImage dismissViewControllerAnimated:YES completion:nil];
                                              }];
[atachImage addAction:cancel];
[self presentViewController:atachImage animated:YES completion:nil];
}

-(void) showCamera{
if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
if (img_Picker == nil){
img_Picker = [[UIImagePickerController alloc] init];}
img_Picker.allowsEditing = YES;
img_Picker.delegate = self;
img_Picker.sourceType = UIImagePickerControllerSourceTypeCamera;
[self presentViewController:img_Picker animated:YES completion:nil];
}else{
UIAlertView *error = [[UIAlertView alloc] initWithTitle:@"Error" message:@"La camara no esta disponible"
                                                                 delegate:nil
                                                        cancelButtonTitle:@"Okay"
                                                            otherButtonTitles:nil];
        [error show];
}
}

-(void) showLibrary{
if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]){
if (img_Picker == nil){
img_Picker = [[UIImagePickerController alloc] init];}
img_Picker.allowsEditing = YES;
img_Picker.delegate = self;
img_Picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
[self presentViewController:img_Picker animated:YES completion:nil];
}else{
        UIAlertView *error = [[UIAlertView alloc] initWithTitle:@"Error" message:@"La biblioteca no esta disponible"
                                                                 delegate:nil
                                                        cancelButtonTitle:@"Aceptar"
                                                            otherButtonTitles:nil];
        [error show];
}
}

#pragma mark - UIImagePickerControllerDelegate 
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)img editingInfo:(NSDictionary *)editInfo {
[self sendMessageWithImage:img];
[picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)cmd_Send_clic:(UIButton*)sender;{
[self sendMessageWithImage:nil];
}

-(void) sendMessageWithImage:(UIImage*)img{
if(_autoReplyMessage != nil) {
[_autoReplyMessage invalidate];
}
ChatData *lastChat = [_chatArray lastObject];
ChatData *chat = [[ChatData alloc]init];
chat.m_iVersion=1;
chat.m_iID=lastChat.m_iID+1;
chat.m_bIsMine=YES;
if(img==nil){
chat.m_eChatDataType=ChatData_Message;
}else{
chat.m_eChatDataType=ChatData_Image;}
chat.m_sMessage=_txt_Message.text;
chat.m_Date=[NSDate dateWithTimeIntervalSinceNow:0];
chat.m_Image=img;
[_chatArray addObject:chat];
_txt_Message.text=@"";
[_tbl_Content reloadData];
[self goToLastMessage];
_autoReplyMessage=[NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(autoReplyMessage:) userInfo:nil repeats:YES];
}

-(void) autoReplyMessage:(NSTimer *) timer {
ChatData *lastChat=[_chatArray lastObject];
ChatData *chat = [[ChatData alloc]init];
chat.m_iVersion=lastChat.m_iVersion;
chat.m_iID=lastChat.m_iID+1;
chat.m_bIsMine=NO;
chat.m_eChatDataType=lastChat.m_eChatDataType;
chat.m_sMessage=lastChat.m_sMessage;
chat.m_Date=lastChat.m_Date;
chat.m_Image=lastChat.m_Image;
[_chatArray addObject:chat];
[_tbl_Content reloadData];
[self goToLastMessage];
[_autoReplyMessage invalidate];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _chatArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
_chatData = _chatArray[indexPath.row];
switch (_chatData.m_eChatDataType) {
case ChatData_Message:
{
if (_chatData.m_bIsMine) {
return [self tableView_MyMessage:tableView cellForRowAtIndexPath:indexPath];
}else{
return [self tableView_OtherMessage:tableView cellForRowAtIndexPath:indexPath];
}
}
break;
case ChatData_Image:
{
if (_chatData.m_bIsMine) {
return [self tableView_MyImage:tableView cellForRowAtIndexPath:indexPath];
}else{
return [self tableView_OtherImage:tableView cellForRowAtIndexPath:indexPath];
}
}
break;
case ChatData_None:
NSLog(@"Error_ChatRoom:tableView--> _chatData with ChatData_None value");
return nil;
default:
break;
}
return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
	[_txt_Message endEditing:YES];
}

- (UITableViewCell *)tableView_MyMessage:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
        static NSString *CellIdentifier = @"Chat_Msg_Cell";
ChatMsgOut_TVC *cell = [_tbl_Content dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)    {
        cell = [[ChatMsgOut_TVC alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.lbl_Message.text = _chatData.m_sMessage;
	cell.lbl_Message.accessibilityLabel = [NSString stringWithFormat:@"Tu mensaje: %@",_chatData.m_sMessage];
    return cell;
}

- (UITableViewCell *)tableView_OtherMessage:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
        static NSString *CellIdentifier = @"Chat_MsgOthers_Cell";
ChatMsgIn_TVC *cell = [_tbl_Content dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)    {
        cell = [[ChatMsgIn_TVC alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.lbl_Message.text = _chatData.m_sMessage;
	cell.lbl_Message.accessibilityLabel = [NSString stringWithFormat:@"Mensaje del contacto: %@",_chatData.m_sMessage];
    return cell;
}

- (UITableViewCell *)tableView_MyImage:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
        static NSString *CellIdentifier = @"Chat_MyImage_Cell";
ChatImgOut_TVC *cell = [_tbl_Content dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)    {
        cell = [[ChatImgOut_TVC alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
	    		    	cell.img_Picture.image = _chatData.m_Image;
												[cell.img_Picture setAccessibilityLabel:[NSString stringWithFormat:@"Tu imagen: %@",_chatData.m_sMessage]];
												    cell.lbl_Footer.text = _chatData.m_sMessage;
        return cell;
}

- (UITableViewCell *)tableView_OtherImage:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
        static NSString *CellIdentifier = @"Chat_OtherImage_Cell";
ChatImgIn_TVC *cell = [_tbl_Content dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)    {
        cell = [[ChatImgIn_TVC alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
	    		    	cell.img_Picture.image = _chatData.m_Image;
						[cell.img_Picture setAccessibilityLabel:[NSString stringWithFormat:@"Imagen del contacto: %@",_chatData.m_sMessage]];
												   cell.lbl_Footer.text = _chatData.m_sMessage;
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Keyboard events

- (void)keyboardWasShown:(NSNotification*)aNotification{
NSDictionary* info = [aNotification userInfo];
CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
[UIView animateWithDuration:0.2f animations:^{
CGRect frame = _view_ToolBar.frame;
frame.origin.y -= kbSize.height;
_view_ToolBar.frame = frame;
frame = _tbl_Content.frame;
frame.size.height -= kbSize.height;
_tbl_Content.frame = frame;
[self goToLastMessage];
}];
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification{
NSDictionary* info = [aNotification userInfo];
CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
[UIView animateWithDuration:0.2f animations:^{
CGRect frame = _view_ToolBar.frame;
frame.origin.y += kbSize.height;
_view_ToolBar.frame = frame;
frame = _tbl_Content.frame;
frame.size.height += kbSize.height;
_tbl_Content.frame = frame;
[self goToLastMessage];
}];
}

- (void) goToLastMessage {
	if (_tbl_Content.contentSize.height > _tbl_Content.frame.size.height)     {
        CGPoint offset = CGPointMake(0, _tbl_Content.contentSize.height -     _tbl_Content.frame.size.height);
        [_tbl_Content setContentOffset:offset animated:YES];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
